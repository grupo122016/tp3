Descripción

    Consigna: cada programa debe registrar por lo menos 4 commit en bitbucket con distintas fechas hasta antes de su presentación en la cátedra virtual.

    1. Construir un programa que lea un archivo .html y genere un archivo .cpp que pueda convertirse a .cgi. Cada línea del html corresponderá a un cout dentro del nuevo programa.

    2. Construir un programa que con la ayuda de una pila sirva para verificar la consistencia de un archivo .html haciendo un push por cada tag de apertura y un pop por cada tag de cierre verificando que lo que devuelve el pop sea lo que el validador esperaba.